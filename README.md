## Synopsis

Python tools for solving the 1d shallow water equations.

## Installation

1. Clone in git `git clone https://<USERNAME>@bitbucket.org/djreich/shallow_water_1d.git`  
2. From **src** folder run `make`, then `make install`

- Be sure to have the *anaconda* distribution of Python.  If it resides somewhere besides your home directory, you will need to edit the **makefile** to provide the correct path to *anaconda*.

## Getting started

We recommend running **dam_break_example.py** and **SPSS.py** to make sure everything is working properly. 
These scripts should be useful examples for solving problems with other initial profiles and boundary conditions