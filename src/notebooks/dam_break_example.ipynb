{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Second-Order Well-Balanced Positivity Preserving Central-Upwind Scheme for the 1D Saint-Venant System\n",
    "## Dam Break Problem\n",
    "The 1D Saint-Venant system is\n",
    "\\begin{align}\n",
    "& h_t+(hu)_x = 0,\\\\\n",
    "& (hu)_t+\\left(hu^2+\\frac12gh^2\\right)_x = -ghB'\\end{align}\n",
    "\n",
    "where $h$ is water depth, $u$ is velocity, $g$ is gravity (9.812 m/$\\text{s}^2$), and $B$ is the bottom elevation (constant in time but not in space)<sup>1</sup>.\n",
    "\n",
    "The dam break problem models the case where a dam separating two levels of water bursts at time t = 0.  This is expressed in terms of piecewise-constant initial data\n",
    "\\begin{equation} h(x,0) = \\begin{cases} h_l & \\text{if $x<0$,} \\\\ h_r & \\text{if $x>0$} \\end{cases} \\quad u(x,0) = 0,\\end{equation} \n",
    "\n",
    "where $h_l>h_r\\ge0$<sup>2</sup>.  Following Example 13.4 from Leveque<sup>2</sup>, we take $h_l=3$ m and $h_r=1$ m, and solve the system numerically using the scheme from the title of this notebook, developed by Kurganov and Petrova<sup>1</sup>.\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "<sup>1</sup> *A. Kurganov and G. Petrova. A Second-Order Well-Balanced Positivity Preserving Central-Upwind Scheme for the Saint-Venant System. (2007). Commun. Math. Sci. Vol. 5, No. 1, pp. 133--160.*\n",
    "        \n",
    "<sup>2</sup> *R. LeVeque. Finite-Volume Methods for Hyperbolic Problems. (2004). Cambridge University Press.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from shallow_water_1dSolvers import ct_upwind_stven\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Setup for the problem.  We let the spatial domain be [-5,5] with a mesh of 1001 nodes (including the boundaries) and use a viscosity parameter of $\\theta = 1$.\n",
    "\n",
    "The initial conditions are set up in the function U0(x), where column 0 is the initial state of $w = h+B$ and column 1 is the initial state of $hu$.  The bottom elevation is the function btm(x), which is just 0 for all x."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "a = -5.; b = 5.; nnodes = 1001; t0 = 0.; theta = 1.; T = [0,0.2,0.5]\n",
    "def U0(x):\n",
    "    u = np.zeros((x.shape[0],2))\n",
    "    xl = np.where(x<0)\n",
    "    xr = np.where(x>=0)\n",
    "    u[xl,0] = 3\n",
    "    u[xr,0] = 1\n",
    "    u[:,1] = 0\n",
    "    return u\n",
    "def btm(x):\n",
    "    return np.zeros_like(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Save initial states for plotting later."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "xgrid = np.linspace(a,b,nnodes)\n",
    "u0 = U0(xgrid)\n",
    "h0 = u0[:,0]+btm(xgrid)\n",
    "hu0 = u0[:,1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Solve up to T = 0.2."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "t,x,w1,hu1 = ct_upwind_stven.ct_upwind(a,b,nnodes,T[1],U0,btm,t0=0,theta=theta,dt=0.001,bc_type=\"dirichlet\")\n",
    "h1 = w1+btm(xgrid)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Confirm that the computation was successful."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Redefine the function for the initial data and solve up to time T=0.5."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def U1(x):\n",
    "    u = np.zeros((x.shape[0],2))\n",
    "    u[:,0] = w1[:]+btm(xgrid)\n",
    "    u[:,1] = hu1[:]\n",
    "    return u\n",
    "t,x,w2,hu2 = ct_upwind_stven.ct_upwind(a,b,nnodes,T[2],U1,btm,t0=T[1],theta=theta,dt=0.001,bc_type=\"dirichlet\")\n",
    "h2 = w2+btm(xgrid)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot depth $h$ and momentum $hu$ side by side for $t=0,0.2,0.5$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(3,2,figsize=(11,8))\n",
    "ax[0,0].plot(x,h0)\n",
    "ax[0,1].plot(x,hu0)\n",
    "ax[1,0].plot(x,h1)\n",
    "ax[1,1].plot(x,hu1)\n",
    "ax[2,0].plot(x,h2)\n",
    "ax[2,1].plot(x,hu2)\n",
    "for i in range(3):\n",
    "    ax[i,0].set_ylim((0.5,3.5))\n",
    "    ax[i,1].set_ylim((-1.,5.))\n",
    "    for j in range(2):\n",
    "        ax[i,j].set_xlim((-5,5))\n",
    "        ax[i,j].set_xlabel('Meters (m)')\n",
    "    ax[i,0].set_title('$h$ at t = '+str(T[i]))\n",
    "    ax[i,1].set_title('$hu$ at t = '+str(T[i]))\n",
    "    ax[i,0].set_ylabel('Depth (m)')\n",
    "    ax[i,1].set_ylabel('Momentum (m$^2$/s)')\n",
    "fig.subplots_adjust(hspace=0.6,wspace = 0.4)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
