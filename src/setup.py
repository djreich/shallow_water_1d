#! /usr/bin/env/python

from distutils.core import setup
from distutils.core import Extension

import numpy

setup(
    name='shallow_water_1d',
    version='0.1.0',
    author='Daniel Reich, Michael Lavigne, Evan North',
    author_email='djreich@ncsu.edu',
    packages=['shallow_water_1dSolvers'],
    description='tools for solving the 1 dimensional shallow water (Saint-Venant) equations',
)
