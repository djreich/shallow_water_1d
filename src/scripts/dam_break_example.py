
# coding: utf-8

# # Second-Order Well-Balanced Positivity Preserving Central-Upwind Scheme for the 1D Saint-Venant System
# ## Dam Break Problem
# The 1D Saint-Venant system is
# \begin{align}
# & h_t+(hu)_x = 0,\\
# & (hu)_t+\left(hu^2+\frac12gh^2\right)_x = -ghB'\end{align}
# 
# where $h$ is water depth, $u$ is velocity, $g$ is gravity (9.812 m/$\text{s}^2$), and $B$ is the bottom elevation (constant in time but not in space)<sup>1</sup>.
# 
# The dam break problem models the case where a dam separating two levels of water bursts at time t = 0.  This is expressed in terms of piecewise-constant initial data
# \begin{equation} h(x,0) = \begin{cases} h_l & \text{if $x<0$,} \\ h_r & \text{if $x>0$} \end{cases} \quad u(x,0) = 0,\end{equation} 
# 
# where $h_l>h_r\ge0$<sup>2</sup>.  Following Example 13.4 from Leveque<sup>2</sup>, we take $h_l=3$ m and $h_r=1$ m, and solve the system numerically using the scheme from the title of this notebook, developed by Kurganov and Petrova<sup>1</sup>.
# 
# 
# 
# 
# 
# <sup>1</sup> *A. Kurganov and G. Petrova. A Second-Order Well-Balanced Positivity Preserving Central-Upwind Scheme for the Saint-Venant System. (2007). Commun. Math. Sci. Vol. 5, No. 1, pp. 133--160.*
#         
# <sup>2</sup> *R. LeVeque. Finite-Volume Methods for Hyperbolic Problems. (2004). Cambridge University Press.*

import numpy as np
from shallow_water_1dSolvers import ct_upwind_stven
import matplotlib.pyplot as plt


# Setup for the problem.  We let the spatial domain be [-5,5] with a mesh of 1001 nodes (including the boundaries) and use a viscosity parameter of $\theta = 1$.
# 
# The initial conditions are set up in the function U0(x), where column 0 is the initial state of $w = h+B$ and column 1 is the initial state of $hu$.  The bottom elevation is the function btm(x), which is just 0 for all x.

a = -5.; b = 5.; n_ints = 1000; t0 = 0.; theta = 1.; T = [0.5,2]
def U0(x):
    u = np.zeros((x.shape[0],2))
    xl = np.where(x<0)
    xr = np.where(x>=0)
    u[xl,0] = 3
    u[xr,0] = 1
    u[:,1] = 0
    return u
def btm(x):
    return np.zeros_like(x)


# Save initial states for plotting later.
dx = (b-a)/n_ints
x = np.linspace(a+dx/2,b-dx/2,n_ints)
btm0 = btm(x)
u0 = U0(x)
w0 = u0[:,0]
hu0 = u0[:,1]


# Solve
t,x,w,hu = ct_upwind_stven.ct_upwind(a,b,n_ints,T,U0,btm,t0=0,theta=theta,bc_type="free")
w1 = w[0]; hu1 = hu[0];
w2 = w[1]; hu2 = hu[1]

T.insert(0,0.)
# Plot depth $h$ and momentum $hu$ side by side for $t=0,0.5,2.0$.
fig, ax = plt.subplots(3,2,figsize=(11,8))
ax[0,0].plot(x,w0)
ax[0,1].plot(x,hu0)
ax[1,0].plot(x,w1)
ax[1,1].plot(x,hu1)
ax[2,0].plot(x,w2)
ax[2,1].plot(x,hu2)
for i in range(3):
    ax[i,0].set_ylim((0.5,3.5))
    ax[i,1].set_ylim((-0.5,2.))
    for j in range(2):
        ax[i,j].set_xlim((-5,5))
        ax[i,j].set_xlabel('x')
    ax[i,0].set_title('$h$ at t = '+str(T[i]))
    ax[i,1].set_title('$hu$ at t = '+str(T[i]))
    ax[i,0].set_ylabel('Water level')
    ax[i,1].set_ylabel('Momentum (m$^2$/s)')
fig.subplots_adjust(hspace=0.6,wspace = 0.4)
fig.savefig('dam_break_example.png')
plt.show()
