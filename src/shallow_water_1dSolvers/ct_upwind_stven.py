import numpy as np
from scipy.integrate import ode,odeint

g = 1.0  # gravity


def ct_upwind(a, b, n_ints, Tlist, u0, btm, t0=0, theta=1., bc_type="free", bc_vals=None):
    """
    Solve the 1d Saint-Venant equations on a uniform grid via the second-order
    well-balanced central upwind scheme of Kurganov and Petrova [KP07]_.

    :param a: lower bound of x domain
    :param b: upper bound of x domain
    :param n_ints: number of cells in discretization
    :param Tlist: list of times at which solutions are returned
    :param u0: function of x giving initial state U = [w,hu] at each x
    :param btm: function btm(x) gives bottom elevation at x
    :param t0: initial time, default t0 = 0
    :param theta: viscosity parameter, 1 <= theta <= 2, default theta=1
    :param bc_type: type of boundary condition, "free", "periodic" or "dirichlet"
    :param bc_vals: only needed if bc_type="dirichlet". bc_vals[0,0]=w(a), bc_vals[0,1] = hu(a), bc_vals[1,0] = w(b), bc_vals[1,1] = hu(b)
    :return: t = final time (integration success if t = Tlist[-1]), 
             x = grid, 
             w = h + B, w[i,:] = water surface elevation on grid x at time Tlist[i]
             hu, hu[i,:] = momentum (discharge) on grid x at time Tlist[i]


    Example
    -------
    Solve the dam break problem.  This problem can be solved with Dirichlet boundary conditions:
    >>>> import numpy as np
    >>>> from shallow_water_1dSolvers import ct_upwind_stven
    >>>> import matplotlib.pyplot as plt

    Make sure to enter a and b as floats.  Domain is [-5,5],
    and we use 1000 cells in the discretization.  Get solutions
    at times 0.5 and 2.
    >>>> a = -5.; b = 5.; n_ints = 1000; t0 = 0.; theta = 1.; T = [0.5,2]

    Define the initial depth profile as 3 if x<0 and 1 if x>=0,
    initial discharge hu = 0 everywhere.
    >>>> def U0(x):
    >>>>     u = np.zeros((x.shape[0],2))
    >>>>     xl = np.where(x<0)
    >>>>     xr = np.where(x>=0)
    >>>>     u[xl,0] = 3
    >>>>     u[xr,0] = 1
    >>>>     u[:,1] = 0
    >>>>     return u

    Take a bottom profile of constant 0.
    >>>> def btm(x):
    >>>>     return np.zeros_like(x)

    Solve.
    >>>> t,x,w,hu = ct_upwind_stven.ct_upwind(a,b,n_ints,T,U0,btm,t0=0,theta=theta,bc_type="free")


    References
    ----------
    .. [KP07] A. Kurganov and G. Petrova, A Second-Order Well-Balanced Positivity Preserving Central-Upwind
        Scheme for the Saint-Venant System. (2007). Commun. Math. Sci. Vol. 5, No. 1, pp. 133--160.

    """

    global g
    dx = (b - a) / n_ints  # delta x.  Make sure a and b are floats (1. instead of 1)
    x = np.linspace(a+dx/2.,b-dx/2.,n_ints) # mesh
    xh = np.linspace(a, b, n_ints+1)  # half mesh
    Bh = btm(xh) # bottom on the half mesh
    y = u0(x)  
    t = t0
    w = np.zeros((len(Tlist),x.shape[0]))
    hu = np.zeros((len(Tlist),x.shape[0]))
    eps = np.sqrt(np.finfo(float).eps)
    for i in range(len(Tlist)):
        T = Tlist[i]
        while t<T-eps:
            f, ap, am = odefun(t,y,Bh,dx,theta,bc_type,bc_vals)
            Ap = max(ap.min(), ap.max(), key=abs)
            Am = max(am.min(), am.max(), key=abs)

            A = max(Ap,-Am)
        
            dt = (1./4.)*dx/A
            dt = min(dt, T - t)   # make sure we land on the final time

            k1 = y + dt*f
            f1, ap, am = odefun(t,k1,Bh,dx,theta,bc_type,bc_vals)
            k2 = 0.75*y + 0.25*k1 + 0.25*dt*f1

            f2, ap, am = odefun(t,k2,Bh,dx,theta,bc_type,bc_vals)
            y = (1./3.)*y + (2./3.)*k2 + (2./3.)*dt*f2
        
            t = t+dt        # update the current time to advance the loop

        w[i] = y[:,0]
        hu[i] = y[:,1]
    return t, x, w, hu


def odefun(t, y, B, dx, theta, bc_type, bc_vals):
    """
    Derivative function for ode solver.

    :param t: time
    :param y: current state of integrand
    :param B: bottom elevation on the half grid
    :param dx: mesh width
    :param theta: viscosity parameter
    :param bc_type: type of boundary condition, "free", "periodic" or "dirichlet"
    :param bc_vals: if bc_type="dirichlet" then bc_vals is 2x2 array with boundary values
    :return: dU/dt
    """


    # Insert ghost points
    w = y[:,0]
    hu = y[:,1]
    n_ints = w.shape[0]
    U = np.zeros((n_ints + 2, 2))
    U[1:-1, 0] = w[:]
    U[1:-1, 1] = hu[:]
    if bc_type == "periodic":
        U[0, :] = U[-2, :]
        U[-1, :] = U[1, :]
    elif bc_type == "dirichlet":
        slope_a = 2*(U[0,:]-bc_vals[0,:])/dx
        slope_b = 2*(bc_vals[1,:]-U[-1,:])/dx
        U[0, :] = U[0,:]-dx*slope_a
        U[-1, :] = U[-1,:]+dx*slope_b
    elif bc_type == "free":
        U[0, :] = U[1,:]
        U[-1,:] = U[-2,:]
    else:
        raise ValueError('bc_type must be "periodic" or "dirichlet"')

    Ux = comp_Ux(U, dx, theta)
    Up, Um = comp_Upm(U, Ux, dx, B)
    ap, am, Up[:,1], Um[:,1] = comp_apm(Up, Um, dx, B)
    H = comp_H(Up, Um, ap, am, B)

    # Equation (2.2) in the paper
    dUdt = - (H[1:] - H[:-1]) / dx
    dUdt[:, 1] = dUdt[:, 1] + comp_S(Up, Um, dx, B)
    
    # Returns the properly shaped dUdt, as well as the ap, am numbers for choosing dt
    #return dUdt.transpose((1,0)).reshape((-1,)), ap, am
    return dUdt, ap, am

def comp_H(Up, Um, ap, am, B):
    """
    Compute numerical flux.

    :param Up: U^plus from Equation (2.5)
    :param Um: U^minus from Equation (2.5)
    :param ap: a^plus from Equation (2.22)
    :param am: a^minus from Equation (2.23)
    :param B: bottom elevation on the half grid
    :return: numerical flux H from Equation (2.3)
    """

    Fm = comp_F(Um, B);
    Fp = comp_F(Up, B)
    qc = np.where(~((ap-am)>0))
    p01 = (ap * Fm[:, 0] - am * Fp[:, 0]) / (ap - am)
    p02 = (ap * am / (ap - am)) * (Up[:, 0] - Um[:, 0])
    p11 = (ap * Fm[:, 1] - am * Fp[:, 1]) / (ap - am)
    p12 = (ap * am / (ap - am)) * (Up[:, 1] - Um[:, 1])
    p01[qc] = 0; p11[qc] = 0; p02[qc] = 0; p12[qc] = 0;
    H = np.zeros_like(Up)
    H[:, 0] = p01 + p02;
    H[:, 1] = p11 + p12
    return H


def comp_F(U, B):
    """
    Auxiliary function to Equation (2.3).

    :param U: physical state U = [w,hu]
    :param B: bottom elevation on the half grid
    :return: F(U,B) following Equation (2.3)
    """
    global g
    hu = U[:, 1]
    w = U[:, 0]
    h = w - B
    f2 = hu * hu / h + g * h * h / 2
    f2[np.where(~(h>0))] = 0
    F = np.zeros_like(U)
    F[:, 0] = hu
    F[:, 1] = f2
    return F


def comp_S(Up, Um, dx, B):
    """
    Compute source discretization.

    :param Up: U^plus from Equation (2.5)
    :param Um: U^minus from Equation (2.5)
    :param dx: mesh width
    :param B: bottom elevation on the half grid
    :return: source discretization S from equation (2.6)
    """
    global g
    p1 = -g * (B[1:] - B[:-1]) / dx
    p2 = (Um[1:, 0] - B[1:]) + (Up[:-1, 0] - B[:-1])
    return p1 * p2 / 2


def comp_Upm(U, Ux, dx, B):
    """
    Compute point values of the piecewise linear reconstruction of U.

    :param U: physical state U = [w,hu]
    :param Ux: spatial derivative of U
    :param dx: mesh width
    :param B: bottom elevation on the half grid
    :return: U^plus,U^minus from equation (2.5)
    """

    Up = U[1:, :] - dx * Ux[1:, :] / 2
    Um = U[:-1, :] + dx * Ux[:-1, :] / 2

    corr_p = np.where(Up[:, 0] < B)
    corr_m = np.where(Um[:, 0] < B)
#    if corr_p[0] or corr_m[0]:
 #       import pdb; pdb.set_trace()
    Up[corr_p, 0] = B[corr_p]
    Up[(corr_m[0] - 1,), 0] = 2 * U[:-1, 0][corr_m] - B[corr_m]
    Um[corr_m, 0] = B[corr_m]
    Um[(corr_p[0] + 1,), 0] = 2 * U[1:, 0][corr_p] - B[corr_p]
    return Up, Um


def comp_apm(Up, Um, dx, B):
    """
    Compute right- and left-sided local speeds.

    :param Up: U^plus from equation (2.5)
    :param Um: U^minus from equation (2.5)
    :param dx: mesh width
    :param B: bottom elevation on the half grid
    :return: a^plus,a^minus from equations (2.22) and (2.23)
             hu^+,hu^minus reconstruction
    """

    global g
    hp = Up[:, 0] - B
    hm = Um[:, 0] - B
    up = np.sqrt(2) * hp * Up[:, 1] / np.sqrt(np.power(hp, 4) + np.maximum(np.power(hp, 4), np.power(dx, 4)))
    um = np.sqrt(2) * hm * Um[:, 1] / np.sqrt(np.power(hm, 4) + np.maximum(np.power(hm, 4), np.power(dx, 4)))
    if (hp<0).any() or (hm<0).any():
        import pdb; pdb.set_trace()
    hup = hp*up; hum = hm*um
    p1 = up + np.sqrt(g * hp)
    p2 = um + np.sqrt(g * hm)
    p3 = up - np.sqrt(g * hp)
    p4 = um - np.sqrt(g * hm)
    return np.maximum(np.maximum(p1, p2), 0), np.minimum(np.minimum(p3, p4), 0),hup,hum


def comp_Ux(U, dx, theta):
    """
    Compute approximation to spatial derivative of physical state U.

    :param U: physical state U = [w,hu]
    :param dx: mesh width
    :param theta: viscosity parameter
    :return: Ux from equation (2.12)
    """

    p1 = theta * (U[2:, :] - U[1:-1, :]) / dx
    p2 = (U[2:, :] - U[:-2, :]) / (2 * dx)
    p3 = theta * (U[1:-1, :] - U[:-2, :]) / dx

    # calculate minimod(p1,p2,p3)
    zmin = np.minimum(np.minimum(p1, p2), p3)
    zmax = np.maximum(np.maximum(p1, p2), p3)
    D = np.zeros_like(zmin)
    D[np.where(zmin > 0)] = zmin[np.where(zmin > 0)]
    D[np.where(zmax < 0)] = zmax[np.where(zmax < 0)]
    Ux = np.zeros_like(U)
    Ux[1:-1, :] = D[:, :]
    # deal with first and last grid points (implemented as "free" bc_type)
    Ux[0,:] = Ux[1,:]
    Ux[-1,:] = Ux[-2,:]
    return Ux
